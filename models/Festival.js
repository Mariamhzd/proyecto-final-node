const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const festivalSchema = new Schema (
    {
        name: { type: String, required: true},
        location: { type: String, required: true },
        date: { type: String, required: true },
        bands: [{ type: mongoose.Types.ObjectId, ref: 'Bands' }],
    },
    {
        timestamps: true,
    }
)

const Festival = mongoose.model('Festival', festivalSchema);

module.exports = Festival;