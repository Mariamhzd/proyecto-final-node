const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const bandSchema = new Schema (
    {
        name: { type: String, required: true},
        image: { type: String },
        gender: { 
            enum: ['Indie nacional', 'Internacional', 'Pop', 'Electro/House'],
            type: String },
        description: { type: String },
        price: { type: String },
    },
    {
        timestamps: true,
    }
)

const Band = mongoose.model('Bands', bandSchema);

module.exports = Band;