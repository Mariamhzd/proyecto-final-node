const express = require ('express');
const path = require('path');
const passport = require('passport');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const indexRouter = require('./routes/index.routes');
const bandRoutes = require('./routes/bands.routes');
const festivalRoutes = require('./routes/festival.routes');
const userRoute = require('./routes/user.routes');

require('./db.js');
require('./passport');

const PORT = 7000;
const server = express();

server.set('views', path.join(__dirname, 'views'))
server.set('view engine', 'hbs');

server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(express.static(path.join(__dirname, 'public')))

server.use(
  session({
    secret: 'mariamartin',
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 360000,
    },
    store: new MongoStore({ mongooseConnection: mongoose.connection })
  })
);

// Añadimos el middleware al servidor
server.use(passport.initialize());

// Añadimos sesiones a nuestros usuarios
server.use(passport.session()); 

server.use("/", indexRouter);
server.use("/bands", bandRoutes);
server.use("/festival", festivalRoutes);
server.use("/users", userRoute);

server.use('*', (req, res, next) => {
  const error = new Error('Route not found');
  error.status = 404;
  next(error);
})

server.use((err, req, res, next) => {
  console.log(err);
  return res.status(err.status || 500).render('error', {
    message: err.message || 'Unexpected error',
    status: err.status || 500,
  })
})


server.listen(PORT, () => {
    console.log(`Server running in http://localhost${PORT}`);
})