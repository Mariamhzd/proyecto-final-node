const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');

const festivalsController = require('../controllers/festivals.controllers');

const router = express.Router();

router.post('/create', [authMiddleware.isAuthenticated], festivalsController.createGet);
//creo método put para añadir los grupos en el festival
router.put('/add-band', festivalsController.addBandPut);

router.get('/',  festivalsController.festivalGet);

router.get('/:id', festivalsController.getfestivalByIdGet);


module.exports = router;