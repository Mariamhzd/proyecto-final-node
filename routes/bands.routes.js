const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const fileMiddleware = require('../middlewares/file.middleware');
const bandsController = require('../controllers/bands.controller');

const router = express.Router();

router.get("/", [authMiddleware.isAuthenticated], bandsController.indexGet);

router.get('/create', bandsController.createdGet)

router.post('/create', [authMiddleware.isAuthenticated], [fileMiddleware.upload.single('image')], bandsController.createdPost);


module.exports = router;