const express = require('express');
const passport = require('passport');

const router = express.Router();

router.post('/register', (req, res, next) => {
    passport.authenticate('register', (error, user) => {
        console.log(error);
        if (error) {
			return res.render('register', { error: error.message });    
        }
        console.log(user);
        return res.redirect('/bands');
    })(req);
});


router.post('/login', (req, res, next) => {
    passport.authenticate('login', (error, user) => {
        if(error) {
            return res.render('login', { error: error.message });
        }

        req.logIn(user, (error) => {
            if(error) {
                return res.render('login', { error: error.message });
            }
            
            return res.redirect('/bands');
        });
    })(req, res, next);
});

router.post('/logout', (req, res, next) => {
    if (req.user) {
      // Destruimos el objeto req.user para este usuario
      req.logout();
  
      req.session.destroy(() => {
        // Eliminamos la cookie de sesión al cancelar la sesión
        res.clearCookie('connect.sid');
        // Redirijimos el usuario a la home
        res.redirect('/');
      });
    } else {
      return res.sendStatus(304); // Si no hay usuario, no habremos cambiado nada
    }
  });

module.exports = router;