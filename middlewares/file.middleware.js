const multer = require('multer');
const path = require('path');

const VALID_FILE_TYPES = ['image/png', 'image/jpg'];

const storage = multer.diskStorage({
  
  filename: (req, file, cb) => {
    console.log(req.file,'ejecuto dentro de filename');
    cb(null, `${Date.now()}-${path.extname(file.originalname)}`);
  },
  destination: (req, file, cb) => {
    console.log(req.file);
    cb(null, path.join(__dirname, '../public/uploads'));
  }
});

const fileFilter = (req, file, cb) => {
  console.log(req.file);
  if (!VALID_FILE_TYPES.includes(file.mimetype)) {
    cb(new Error('Invalid file type'));
  } else {
    cb(null, true);
  }
}

const upload = multer({
  storage,
  fileFilter,
  limits: {
    fieldNameSize: 500,
    fieldSize: 500000000,
  },
});


module.exports = { upload };
