const Band = require('../models/Band');

const indexGet = async (req, res) => {
    try {
      const bands = await Band.find();
  
      return res.status(200).render('bands', {title: 'Bands List', bands });
      
    } catch (error) {
      return res.status(500).json(error);
    }
};

const createdGet = (req, res, next) => {
  res.status(200).render("create-band", { title: 'Create new band'});
};

const createdPost = async (req, res, next) => {
    try {
      const {
        name,
        // image,
        gender,
        description,
      } = req.body;
      console.log(req.body)
      const newBand = new Band({
        name,
        image: req.file? `/uploads/${req.file.filename}` : null,
        gender,
        description,      
      });
      //para guardar la banda en la bd
    const createdBand = await newBand.save();
    return res.status(200).json(createdBand);
  } catch (err) {

    next(err);
  }
};

module.exports = {
    indexGet,
    createdGet,
    createdPost
}