const Festival = require('../models/Festival');
//ruta para crear
const createGet = async (req, res, next) => {    
    console.log(req.body);
    try {
        const { name, location, date } = req.body;

        const newFestival = new Festival({
            name,
            image,
            location,
            date,
            bands: [],
        })
        const createdFestival = await newFestival.save();

        return res.status(201).json(createdFestival);

    } catch(error) {
        next(error);
    }
};

const addBandPut = async (req, res, next) => {
    try {
        const { festivalId, bandId } = req.body;

        const updateFestival = await Festival.findByIdAndUpdate(
            festivalId,
            { $push: { bands: bandId }},
            { new: true }            
        );

        return res.status(200).json(updateFestival);

    } catch(error) {
        next(error);
    }

};

const getfestivalByIdGet = async (req, res, next) => {
    try {
        const { id } = req.params;
        const festival = await Festival.findById(id).populate("bands");
        console.log(festival);

        return res.status(200).render('festival', { title: 'Festival', festival });
        
    } catch (error) {
        next(error);
    }
};

const festivalGet = async (req, res, next) => {
    try {
        const festival = await Festival.find().populate('bands');

        return res.status(200).render('festival', { title: 'Festival', festival });
    } catch (err) {
        next(err);
    }
};

module.exports = {
    createGet,
    addBandPut,
    getfestivalByIdGet,
    festivalGet,
}