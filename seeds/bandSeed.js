const mongoose = require('mongoose');
const DB_URL = require('../db');
const Band = require('../models/Band');

const bands = [
    {
        name: 'Vetusta Morla',
        image: "https://static2.eldiariomontanes.es/www/multimedia/201912/17/media/cortadas/vetusta-morla-ksJC-U909823889691oC-1248x770@Diario%20Montanes.jpg",
        gender: 'Indie nacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'Dorian',
        image:'https://static2.abc.es/media/cultura/2018/11/06/dorian-riviera-justicia-universal-ketH--620x349@abc.jpg',
        gender: 'Indie nacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'Love of Lesbian',
        image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT72hWES_FXagE3AQWedWotrQjj5UnLbr6GKA&usqp=CAU',
        gender: 'Indie nacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'La M.O.D.A',
        image: 'https://static4.abc.es/media/cultura/2017/11/24/moda-kzDC--620x349@abc.jpg',
        gender: 'Indie nacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'Los Planetas',
        image: '',
        gender: 'Indie nacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'Artic Monkeys',
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTW1TTXrd1i5yn8_yr1qWOMz5VWNqx7qyjNkw&usqp=CAU',
        gender: 'Internacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'The Killers',
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpf7XxucxeCWejqDNkIjMXMcIWchjzieugHQ&usqp=CAU',
        gender: 'Internacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'Franz Ferdinand',
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjmVIzGhg88Z7DmtmRisATpIZBtTc6stF5AQ&usqp=CAU',
        gender: 'Internacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'Imagine Dragons',
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT2Jw0OG3dQoyDDEjZvAmjK59lVV3J92hro3A&usqp=CAU',
        gender: 'Internacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'The Cure',
        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAScAAACrCAMAAAATgapkAAAAflBMVEX///8AAAC7u7vx8fH29vbn5+fr6+v8/PwFBQUrKytdXV1wcHDj4+OIiIj5+fnW1taRkZHJyclBQUF6enqpqalVVVV+fn7FxcWwsLAkJCQwMDALCwva2tqgoKBzc3M1NTVlZWWMjIyZmZk/Pz8WFhZLS0sZGRloaGhHR0ceHh6dSHRSAAAJz0lEQVR4nO2b6XqqOhSGDVNkBmUUUFHrcP83eEgCCCEo9djqtuv900eMFD5W1pQwmwEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAv4xqybcHKJaKf+dS3hgspSh0OweUZCcp3RHqFmWFMvvjSBuEkHG1KGV3RHrRsR/Hrgak0gsu7Z3QFpUKSFfbA/68+pxZnREhGRHuPM+T7kzQD0Yj5oRWHZ3I5/lVJ7y7oIbUjH2C9oorfS3KkgiQJe0BXicrQzxz8wOFws7t7/0VufWoHcXrlA9kqvA+Lv7ldpSoPThbUNZUp/bGeZ3kk0Cn9R3x/zncyglneo993DMGZ3lbp5lF/fh+uzX0Vqfi0+zJExjDvpst3ddJNsiRUpFlycjmhCz6oLhX3Ve8Kw8i75J0x03UaUfPaVHUW7MOy/+QiHLuR9vtSiQSQodezvgdnaYQbE9J7lbk76+Xtp2LJSJwNQjTadke+x86yYHn2eQ/k8mJ5ut3F0pdctKYUoe4X6oxnVZ+85nm4w/pJJdcqmW/eZoV8CZ06yZxQm0vkDWKLNGP+XXAZJ3igelG6uhgRXu9tRXNdaZhSIsO79ZoNaUGtajZMAvsJFBG1yTHTcQNBzrNF0UsliM394Yr/OYXYTqFnpdoljFRJ55TOzuZTu1cGjMDtZWJ5g6tgyxFP1DIOb8swTe/SUwC3YnavLy9r5OOBBzbKdPXCRUjp3Hpt7pRRzu3bFRbCoSS6Ff+8ItfxUkyZDAPw27ya9xPVKPXIp30doJxOpUjp3FrN6fUGYZsNr/wBj0+VisdXq1Tp5XrsIT8pisYuP2KzTXH6us01rDDbLJ3/f9uUf9mYFEK1TDqp6uO/MqeKX3M8/s66UUSEJKAynK63hrT6ciKw1UwkouzCZ52XQ52vTrT9TihmE5275jmGcsXOqypOp3bW6GOxbw+W6aTrbFmw1gBzLqeNnfUqjO5pH9YoBOdpuHrhJqq06L1R2Kd7uVPyUU4SmNCGX2DGuqkRXRcfOe//By/pdOODFoN75O581U/kAx0kutYwv1e0267LKxpz2qEPUun0qdpZi48QxM0Q0EKEOv37am2unk/OcP+eb9syy1ByHaS82I0n/smz9Jpz1YTvkbOZB0E04viJIf++uBsoJPMJh3i2lp+Ey+phkYycI0SeQS751jUmE5OnuOZ4xJ1mB9Xc1eluY9Yp4btSB1CsydhzoBzly92OJ127NQn7tRcvnIoeEW8Gxf0XcZ00jYbbWZRt8PsqUqRN2vPr7P5zsPr6/Qlviy6KjOfmjj2dbLO9MwZr3KB+qQBZ1FUp5OiWtaE/uEdejphJzHtisrL4Ei3ZjnVySel79K/6FXRHJHP63X3krn6TuhZcUK/fEwn1mfY8jKxlcUuGfe4qU57+zKvycxiVzyYWvR0KkPWB67CCrZ1dabRas2RVqlpySl5MtRaFKX74HBSN4/TKs9ciysgduPp1A5ATyeVhoChybP6/MLyW1arX6XUSMOUb7NRzFs12jisPK0vYrXaIF/WaLJYIHMXVa6T2KqmKjMt/YpjseEmxF+adl7lmSNxWliIjNPTiVVW2cAOWH0e0PxW8w89X+SGqNOS6HOz6B8lJ95mVV9EYEnt3JDQYaWjsO1iays0R+Kwj6v4W8q3lqLYjY+VyCPDmU501qNDOdCY6VSbEMs7mniaD1tdVyZfRQ8sbbpFrd+WED4qZStbt99oqUFCoPgk9/K5x3WK98wIhoaaZF2dyo5O2Oak6VqW/uDOGuz6ndu/6hRXPhwfuzrp2/WjFfvDOtXOejP0vg5Nquq+fW5exnS6fIUF3RziS0ZYIT1lCTZudVKN6hI6+bVyRquHc5GHdKqGOyz2rwQ2wHpUJ+IU1bwOuc31ScSt1/7JsFqPIBOes1JtRWOVx8zqFVeKG/vTQ+xDOhlBElH3vBGVvzG1oEMn2qF9m3dI222Qs3i3/ZmulTJNb9lO0TysNXXumhm78eXUdSil7XVSbNE1SQMH3ZETU7t5Zj7OM9EsE/RlRyu2aoDtMxHg1nNT2KaWxcS+SF+nk0heh/fV6DzIYn9Sp4msD7njWCean8shKVb8W8u7Sl3JnuPcHZ3YneE9nRLREG3BybQZJvuv10lJQ/p3vZkRnao4k6eoSnSdIKiX8Pz+3bnNUvAcpaUr39mL0ddJGMitpmrJshFregedLGpIVVAkibwchopi0N0bdHOdUc1dzczssizaYqO3aLMPQyPxx5KxGaeTuBQqGuHPCW0/uYKzMZ0UjSQFr1HLR8H1Q2VPlb/y2PEyj6oqqNiw2zCuvxhUrYuq4C5dTbM8mxJc9ejqlAqj6jVFurF6RXUKg7BKEDLzJS1jTqdQSlmrMkCltMjyWblIj4nlJp3iNR4IRVgtFvtmAi3ajXw9ncR169WN802+K1SnQ12l689JML+Hi86S25gyaaxc2FMl93cJSFxe6/z9SSPbrTrzsTGNrk5LYRx1ouuIcCzd6G8WFGZhPwyuUrjLwvR2URRTnQxa38lnpCc0njnreWhwDjjeozs0a+7Orj20FYvg0yyzTjD5/lwDt6ly8la2J6LZx/q/m9RHM0nc1T5jSWFVVRzQqf8b7K8PN/anVbGwncx5O06YFDS9u4B589XIoFanee8p/C5KYp7C8zms5oof1heaZFJS79Q0M9cZzBisWsV5XKer7bSb90UrNASqU6bWbmrYnKLUOm2i3D/f2Enzw2BFkTW6mws3xWUwd+WvlHgZHCEfixaHcb4b81PHq6d1agc16qObt2zcLzpOrAHT6ULe7nKD5I0272mxUmWUNAX1j5tQF1u6tYuM4cse/dlDo6MejVXabPsIMSOX2adwYz8dtHnXN7lOiMw3HFVCjUQYjNV6cTJatJx7cRtL5/05GW3+yWE73ZjLP4qyqNfn47coUubJ1Qk7LB3tCveVpo3u6ah0otPNIA+kXgQVpQ9UJ+ON5lsPyWR/fzKtYzqxaa3RXsRlsMpZ63QZCYZ/Ao3qVCdEUsbc9WAU3bqAwj/8Si59K7BZEagXWocTjL07iGgwcBRFmdiD/CSsLcqCxlDYjuv50KBYz3NnWVZemqa5ftfQ94MoVqd+zGmtO6zzctbMm2cZy8gPf1CoLph2t44Dj4351+aWn/b+5DdhL+cODcrimjmTV+4/FNZ/H+7V4Pa2C3rof4zedpLucZpAEO8U2nb552WqGzGCqtkqQrZu/th2nk8jJ7kmv0+Mgp+1VP4R4CIVli4AhyLtkj9coAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8E78By00hMJ/4cMfAAAAAElFTkSuQmCC',
        gender: 'Internacional',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'Amaral',
        image: '',
        gender: 'Pop',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
    {
        name: 'David Guetta',
        image: '',
        gender: 'Electro/House',
        description: 'lorem ipsum',
        price: 'xxxxxxx €',        
    },
]

mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true, 
 })
 .then(async () => {
     const allBands = await Band.find();
 
     if(allBands.length) {
         await Band.collection.drop();
     }
 })
 .catch((err) => {
     console.log(`Error deleting db data ${err}`);
 })
 .then(async () => {
     await Band.insertMany(bands);
 })
 .catch((err) => {
     console.log(`Error adding data to our db ${err}`)
 })
 .finally(() => mongoose.disconnect());