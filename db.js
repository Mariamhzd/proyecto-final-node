const mongoose = require('mongoose');

const DB_URL = 'mongodb://localhost:27017/proyecto-final-node';

mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
.then( () => {
  console.log('Success connecteded to DB');
})
.catch(() => {
  console.log('Error connecting to DB');
})

module.exports = DB_URL;